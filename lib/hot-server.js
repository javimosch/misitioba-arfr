var http = require('http');
var express = require('express');
var app = express();
var path = require('path');
var resolver = require(path.join(process.cwd(), 'lib/resolver'));

module.exports = {
    run: () => {
        app.use(require('morgan')('short'));

        // ************************************
        // This is the real meat of the example
        // ************************************
        (function() {

            // Step 1: Create & configure a webpack compiler
            var webpack = require('webpack');
            var webpackConfig = require(resolver.env().WEBPACK_CONFIG);
            var compiler = webpack(webpackConfig);

            if (!webpackConfig.output || !webpackConfig.output.publicPath) {
                throw Error('webpackConfig.output.publicPath required');
            }

            // Step 2: Attach the dev middleware to the compiler & the server
            app.use(require("webpack-dev-middleware")(compiler, {
                noInfo: true,
                //           publicPath: webpackConfig.output.publicPath
                publicPath: "/dist", // Do not end publicPath with a / 
                watchOptions: {
                    poll: true
                },
                stats: {
                    colors: true
                }
            }));

            // Step 3: Attach the hot middleware to the compiler & the server
            app.use(require("webpack-hot-middleware")(compiler, {
                log: console.log,
                path: '/__webpack_hmr',
                heartbeat: 10 * 1000
            }));
        })();

        // Do anything you like with the rest of your express application.

        //MIDDLEWARES, ROUTES AND SERVER
        var logger = resolver.logger().get('HOT-SERVER', 'BOOTSTRAP');
        resolver.middlewares().configure(app, express);
        resolver.routes().configure(app, express);
        require(path.join(process.cwd(), 'lib/backend')).configure(app).then(() => {});
        require('http').Server(app).listen(resolver.env().PORT, function() {
            logger.debugTerminal('Production? ' + (resolver.env().PROD ? 'YES!' : 'NO!'));
            logger.debugTerminal('ROLLING ON PORT ' + resolver.env().PORT + '!', "http://localhost:" + resolver.env().PORT);
        });

    }
};
