const path = require('path');
const resolver = require(path.join(process.cwd(), 'lib/resolver'));
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const AsyncAwaitPlugin = require('webpack-async-await');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractSass = new ExtractTextPlugin({
  filename: "[name].[contenthash].css",
  disable: process.env.NODE_ENV === "development"
});

let output =  {
    filename: resolver.env().JS_BUNDLE_PATH,
    path: path.join(process.cwd(), resolver.env().OUTPUT_FOLDER),
    publicPath: resolver.env().PUBLIC_PATH
  };
  
console.log('OUTPUT',output);

module.exports = {
  target: 'web',
  entry: [
    // Add the client which connects to our middleware
    // You can use full urls like 'webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr'
    // useful if you run your app from another point like django
    'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
    //
    resolver.clientFolder('js/main')
  ],
  resolve: {
    alias: {
      js: resolver.clientFolder('js'),
      res: resolver.clientFolder('res'),
      img: resolver.clientFolder('res/img'),
      common: resolver.commonFolder(),
    }
  },
  devtool: process.env.NODE_ENV === 'production' ? false : "eval",
  output:output,
  plugins: [
    new CleanWebpackPlugin([path.join(process.cwd(), resolver.env().OUTPUT_FOLDER)], {
      dry: true,
      verbose: true
    }),
    new AsyncAwaitPlugin({
      awaitAnywhere: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    extractSass
  ],
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader?cacheDirectory',
          options: {
            presets: ['env'],
            plugins: ['transform-runtime']
          }
        }
      }, {
        test: /\.scss$/,
        use: [{
          loader: "style-loader" // creates style nodes from JS strings
        }, {
          loader: "css-loader" // translates CSS into CommonJS
        }, {
          loader: "sass-loader" // compiles Sass to CSS
        }]
      }, {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }, {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      }, {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      }, {
        test: /\.(csv|tsv)$/,
        use: [
          'csv-loader'
        ]
      },
      {
        test: /\.xml$/,
        use: [
          'xml-loader'
        ]
      }
    ]
  }
};
