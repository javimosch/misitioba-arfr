//THIS MODULE IS CALLED DURING DEVELOPMENT TO HANDLE GENERATOR
require('dotenv').config({
    silent: true,
    path: process.cwd() + '/.env'
});

if(!process.env.APP_NAME && !process.env.APP) throw new Error('env APP_NAME or APP required');


var resolver = require('./resolver');



var logger = resolver.logger().get('DEV:CLIENT', 'BOOTSTRAP');
console.log("TRACE")
if (!resolver.env().PROD) {
    resolver.co(function*() {
        var logger = resolver.logger().get('DEV:CLIENT', 'BOOTSTRAP');
        logger.debugTerminal('GENERATOR:START');
        yield resolver.generator().configure();
        //yield resolver.generatorBags().configure(app);
        resolver.generator().logger().debugTerminal('GENERATOR:OK');
        return yield resolver.Promise.resolve(true);
    }).catch(resolver.errorHandler(logger.errorTerminal));

    if (resolver.env().WEBPACK) {
        resolver.hotServer().run();
    }

}
else {
    logger.debugTerminal('GENERATOR:OFF');
}
