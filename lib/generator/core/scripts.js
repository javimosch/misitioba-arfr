"use strict";

//var gulp = require('gulp');
//var file = require('gulp-file');
var rollupWrapper = require('rollup');
//var webpackStream = require('webpack-stream');
//var webpack2 = require('webpack');
var babel_plugin = require('rollup-plugin-babel');
var resolve = require('rollup-plugin-node-resolve');
var commonjs = require('rollup-plugin-commonjs');
var minifyHarmony = require('uglify-js-harmony').minify;
//var buble = require('rollup-plugin-buble');
//var heParser = require('./utils.html-parser');
//var source = require('vinyl-source-stream');
var heUtils = require('./utils');
//var minify = require('minify-content');
var path = require('path');
var resolver = require(path.join(process.cwd(), 'lib/resolver'));
var heConfig = resolver.handlebarsContext();
var babel = require("babel-core");
var PROD = resolver.env().PROD;
var uglify = require('rollup-plugin-uglify');
var logger = resolver.logger().get('GENERATOR', 'SCRIPTS');
var fs = require('fs');
var builtins = require('rollup-plugin-node-builtins');
var rollupGlobals = require('rollup-plugin-node-globals');
var shelljs = require('shelljs');
//var UglifyJS = require("uglify-js");
var g = {
	destFilename: 'app.js',
	rollupCache: null
};
var PATH = './src/client/' + resolver.env().APP_NAME + 'js';
var DIST = './dist/js';
var DEST_FOLDER = 'js';
let buildInProgress = false;

var includePaths = require('rollup-plugin-includepaths');

function handleError(msg, err) {
	buildInProgress = false;
	logger.errorTerminal(msg, err);
}

function watch() {

	if (resolver.env().WEBPACK) {
		//return;
	}

	logger.debugTerminal('watch', PATH);
	heUtils.watch(PATH, () => {
		build().catch(resolver.errorHandler((err) => handleError('Watch Build:', err)));
	});

	heUtils.watch(path.join(resolver.CONSTANT().SRC_CLIENT_PATH, resolver.env().APP_NAME, 'context.js'), () => {
		build().catch(resolver.errorHandler((err) => handleError('Watch Build (Context):', err)));
	});

	heUtils.watch(resolver.CONSTANT().CLIENT_COMMON_JS_PATH, () => {
		logger.debugTerminal('Build triggered by common');
		build().catch(resolver.errorHandler((err) => handleError('Watch Build (Common):', err)));
	});

}

function build() {
	return resolver.coWrapExec(function*() {

		if (resolver.env().WEBPACK) {

			logger.debugTerminal('WEBPACK Call');
			let webpackConfigPath = path.join(process.cwd(), 'lib/webpack.config.js');
			var child = shelljs.exec(path.join(process.cwd(), "node_modules/.bin/webpack --config " + webpackConfigPath), { async: true });
			child.stdout.on('data', function(data) {
				//logger.debugTerminal('WEBPACK', data);
			});

			var nodeCleanup = require('node-cleanup');

			nodeCleanup(function(exitCode, signal) {
			//	child.exit(0);
			});

			return resolver.Promise.resolve(true);
		}

		if (!resolver.env().BUILD_SCRIPTS) {
			logger.debugTerminal('SKIP (BUILD_SCRIPTS disabled)');
			return resolver.Promise.resolve(true); //already in progress
		}

		if (buildInProgress) {
			logger.debugTerminal('SKIP (Already in progress)');
			return resolver.Promise.resolve(true); //already in progress
		}
		else {
			buildInProgress = true;
		}


		var exists = yield resolver.getFacade('fs').exists(path.join(PATH, 'main.js'));
		if (!exists && resolver.env().ROLLUP) {
			logger.warnTerminal("Rollup expects main.js at", PATH);
		}
		if (exists && resolver.env().ROLLUP) {
			let pkg = JSON.parse(fs.readFileSync('./package.json')),
				external = Object.keys(pkg.dependencies || {});

			var rollupExternal = resolver.handlebarsContext()().rollupExternal || [];
			logger.debugTerminal('Externals', rollupExternal);



			var startDate = Date.now();
			logger.debugTerminal("Rollup start");
			/*ROLLUP ===========================================*/
			var bundle = g.rollupCache = yield rollupWrapper.rollup({
				entry: path.join(PATH, 'main.js'),
				cache: g.rollupCache,
				external: ['d33'],
				plugins: [
					rollupGlobals(),
					builtins(),
					includePaths({
						include: {},
						paths: [path.join(process.cwd(), 'src/client/common/js/components')],
						external: [],
						extensions: ['.js', '.json', '.html']
					}),
					babel_plugin({
						plugins: [
							["transform-react-jsx", { "pragma": "h" }],
							/*["module-resolver", {
								"root": ["."],
								"alias": {}
							}]*/
						],
						presets: [
							[
								'env', { "modules": false }
							]
						],
						babelrc: false,
						exclude: 'node_modules/**'
					}),
					resolve({
						jsnext: true,
						main: true,
						externals: external,
						extensions: ['.js', '.json'],
					}),
					commonjs({
						include: 'node_modules/**',
						exclude: '**/*.css'
					}),
					resolver.env().PROD && uglify({}, minifyHarmony)
				],
			});
			logger.debugTerminal("ROLLUP BUNDLED", ((Date.now() - startDate) / 1000).toFixed(2) + 'sec');
			startDate = Date.now();

			try {
				yield bundle.write({
					paths: {
						d33: 'https://d3js.org/d3.v4.min'
					},
					format: 'iife',
					indent: false,
					dest: path.join(heConfig().output(DEST_FOLDER), g.destFilename),
					sourceMap: (resolver.env().PROD === false) ? 'inline' : false
				});
			}
			catch (err) {
				return resolver.errorHandler(logger.error);
			}



			logger.debugTerminal("ROLLUP WRITE DOWN", ((Date.now() - startDate) / 1000).toFixed(2) + 'sec');

			if (resolver.env().PROD) {
				//var generated = bundle.generate();
				//var code = generated.code;
				logger.debug("Read output and uglify");
				/*
				var result = UglifyJS.minify(code);
				if (result.error) {

					logger.error("Uglify error", result.error);
				}
				else {
					yield heUtils.createFile(path.join(heConfig().output(DEST_FOLDER), g.destFilename), result.code);
				}*/
			}

			if (resolver.env().HOT_MAQUETTE) {
				var code = bundle.generate().code;
				resolver.generatorFirebase().sendHotScript(code);
				logger.debugTerminal('HOT_MAQUETTE on', code.length);
			}
			else {
				logger.debugTerminal('HOT_MAQUETTE off');
			}

			logger.debugTerminal("Rollup finish");

			return success(resolver.env().HOT_MAQUETTE);
		}
		else {
			var raw = heUtils.concatenateAllFilesFrom(PATH, {
				debug: false
			});
			if (PROD) {
				var settings = {
					presets: ["es2015"],
					minified: true,
					comments: true
				};

				if (process.env.PROD_DEPLOY && process.env.PROD_DEPLOY.toString() == '1') {
					settings.comments = false
				}
				else {
					settings.sourceMaps = 'inline';
				}

				var r = babel.transform(raw, settings);
				return build_next(r.code);
			}
			else {
				return build_next(raw);
			}
		}

		function build_next(_raw) {
			heConfig().jsVendorFileName = g.destFilename;
			var dest = heConfig().output(DEST_FOLDER + '/' + g.destFilename);
			heUtils.createFile(dest, _raw);
			return success();
		}

		function success(wasHotReload) {
			buildInProgress = false;
			logger.debugTerminal('Build ' + g.destFilename + ' success');
			wasHotReload = wasHotReload || false;


			emit('build-success', {
				wasHotReload: resolver.env().HOT_MAQUETTE
			});
			return resolver.Promise.resolve(true);
		}

		function handleError() {
			buildInProgress = false;
			logger.debugTerminal('Build ' + g.destFilename + ' fail');
			return resolver.Promise.resolve(false);
		}

	});
}


function emit(evt, p) {
	_events[evt] = _events[evt] || [];
	_events[evt].forEach(handler => handler(p));
}
var _events = {};



function getAll() {
	return heUtils.retrieveFilesFromPathSync(PATH);
}

function tagTemplate(context) {
	var rta = heUtils.replaceAll('<script type="text/javascript" src="_SRC_"></script>', '_SRC_', context.src || '[src_field_required]');
	return rta;
}

function printTags(folderPath) {
	var files = getAll();
	var ret = "<!-- printJSTags: development -->\n";
	files.forEach((file) => {
		ret += tagTemplate({
			src: path.join(folderPath, file.fileName)
		});
	});
	return ret;
}

module.exports = {
	path: (p) => PATH = p,
	dest: (dest) => DIST = dest + '/' + DEST_FOLDER,
	build: build,
	tagTemplate: tagTemplate,
	printTags: printTags,
	watch: watch,
	on: (evt, handler) => {
		_events[evt] = _events[evt] || [];
		_events[evt].push(handler);
	}
};
