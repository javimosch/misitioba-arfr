var path = require('path');
var resolver = require(path.join(process.cwd(), 'lib/resolver'));
var heBuild = require('./main').build;
var heWatch = require('./main').watch;
var heOptions = require('./main').options;
var heFirebase = require('./core/firebase');
var heUtils = require('./core/utils');
const APP_NAME = resolver.env().APP_NAME;
var heConfig = resolver.handlebarsContext();
var logger = resolver.logger().get('GENERATOR', 'ENTRY');

const OUTPUT_PATH_FOR_DEVELOPMENT = path.join(process.cwd(), 'dist');

module.exports = {
  configure: resolver.coWrap(function*() {

    logger.debugTerminal('Configuring..');

    heUtils.ensureDirectory(OUTPUT_PATH_FOR_DEVELOPMENT);
    heUtils.ensureDirectory(process.cwd() + '/dist-production');

    yield heOptions.setApp(APP_NAME);



    heOptions.dest('dist', 'dist-production');

    logger.debugTerminal('CURRENT APP_NAME ->', APP_NAME);
    logger.debugTerminal('outputLanguageFolder', heConfig().outputLanguageFolder());
    logger.debugTerminal('outputBaseDir', heConfig().outputBaseDir());
    logger.debugTerminal('output', heConfig().output());

    logger.debugTerminal('Cleaning');
    yield resolver.fsFacade().rimraf(path.join(OUTPUT_PATH_FOR_DEVELOPMENT, '**/*'));
    logger.debugTerminal('Cleaning OK');

    logger.debugTerminal('BUILD START');
    yield heBuild.all({
      scripts: true,
      templates: true,
      styles: true
    });
    logger.debugTerminal('BUILD SUCCESS');
    if (resolver.env().DISABLE_WATCH) {
      logger.warn('Watch is disabled');
      return;
    }
    heFirebase.sendSignal('reload', {
      full_reload: true
    });
    logger.debugTerminal('Watch templates');
    heWatch.templates();
    logger.debugTerminal('Watch scripts');
    heWatch.scripts();
    logger.debugTerminal('Watch styles');
    heWatch.styles();
    return yield resolver.Promise.resolve(true);
  })
};
