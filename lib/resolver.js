Error.stackTraceLimit = Infinity;
require("better-stack-traces").register();
var path = require('path');
var reload = require('require-reload')(require);
var Promise = require('promise');
var co = require('co');
var delay = require('timeout-as-promise');
require(path.join(process.cwd(), 'lib/config/env'));
const util = require('util');
var CONSTANTS = require(path.join(process.cwd(), 'lib/constants'));
var self = {
    hotServer: () => {
        return require(path.join(process.cwd(), 'lib/hot-server'));
    },
    reloadRequire: function() {
        reload.apply(reload, arguments);
    },
    errorHandler: (h) => (err) => {
        //let msg = (err.stack ? err.message + ' STACK: ' + err.stack : err);
        //let msg = util.inspect(err);
        
        console.log('ERROR', err);
        
        //h(msg);
    },
    pathTo: (relativePath, joinPath) => path.join(process.cwd(), relativePath, joinPath || ''),
    backendDatabase: () => {
        return require(path.join(process.cwd(), 'lib/backend/database/backend-database'));
    },
    model: () => self.backendDatabase().model,
    backendRouter: () => {
        return require(path.join(process.cwd(), 'lib/backend/backend-router'));
    },
    backendRoutes: () => {
        return require(path.join(process.cwd(), 'src/server/routes'));
    },
    backendTasks: () => {
        return require(path.join(process.cwd(), 'backend/model/tasks'));
    },
    fsFacade: () => self.getFacade('fs'),
    sessionModule: () => {
        return require(path.join(process.cwd(), 'lib/sessions'));
    },
    delay: (milliseconds) => delay(milliseconds),
    mongoose: () => require('mongoose'),
    gfs: () => self.db().state().gfs,
    promise: (handler) => new Promise(handler),
    Promise: Promise,
    http: () => require('request'),
    _: () => require('lodash'),
    co: (handler) => co(handler),
    coWrap: (handler) => co.wrap(handler),
    coWrapExec: (handler) => co.wrap(handler)(),
    database: () => {
        return require(path.join(process.cwd(), 'lib/database'));
    },
    logger: () => {
        return require(path.join(process.cwd(), 'lib/logger'));
    },
    generator: () => {
        return require(path.join(process.cwd(), 'lib/generator'));
    },
    generatorEntryPoint: () => {
        return require(path.join(process.cwd(), 'lib/generator/entry_point'));
    },
    generatorTemplates: () => {
        return require(path.join(process.cwd(), 'lib/generator/core/templates'));
    },
    middlewares: () => {
        return require(path.join(process.cwd(), 'lib/middlewares/middlewares'));
    },
    routes: () => {
        return require(path.join(process.cwd(), 'lib/routes/routes'));
    },
    ctrl: () => {
        return require(path.join(process.cwd(), 'lib/backend/database/backend-controllers-manager')).getAll();
    },
    ctrlManager: () => {
        return require(path.join(process.cwd(), 'lib/backend/database/backend-controllers-manager'));
    },
    handlebarsContext: () => {
        return require(path.join(process.cwd(), 'lib/config'));
    },
    asyncWait: milliseconds => new Promise(resolve => setTimeout(_ => resolve(true), milliseconds)),
    env: () => {
        return require(path.join(process.cwd(), 'lib/config/env'));
    },
    generatorUtils: () => {
        return require(path.join(process.cwd(), 'lib/generator/core/utils'));
    },
    generatorFirebase: () => {
        return require(path.join(process.cwd(), 'lib/generator/core/firebase'));
    },
    generatorBags: () => {
        return require(path.join(process.cwd(), 'lib/generator/core/bags'));
    },
    utils: () => {
        return require(path.join(process.cwd(), 'lib/utils/utils'));
    },
    getFacade: (relativePath) => {
        return require(path.join(process.cwd(), 'lib/facades/' + relativePath + '-facade'));
    },
    CONSTANT: () => CONSTANTS,
    require: (relativePath, joinPart) => {
        return require(path.join(process.cwd(), relativePath, joinPart || undefined));
    },
    clientFolder: function() {
        var arr = Array.prototype.slice.call(arguments);
        arr.unshift(path.join(process.cwd(), self.CONSTANT().SRC_CLIENT_PATH, self.env().APP_NAME));
        return path.join.apply(path, arr);
    },
    commonFolder: function() {
        var arr = Array.prototype.slice.call(arguments);
        arr.unshift(path.join(process.cwd(), self.CONSTANT().CLIENT_COMMON_PATH));
        return path.join.apply(path, arr);
    }
};
module.exports = self;
