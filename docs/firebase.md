#Remove this line and move it to copy to the root folderas firebase.json
{
    "hosting": {
        "public": "dist-production",
        "ignore": [
            "firebase.json",
            "**/.*",
            "**/node_modules/**",
            "src",
            ".*"
        ]
    }
}
