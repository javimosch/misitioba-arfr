import { h, render, Component } from 'preact';

// Indica a Babel tranforme las llamadas de la función h() a JSX:
/** @jsx h */

//import Button from 'Button';

//import JsEditor from 'JsEditor';

render((
    <div id="foo">
        <span>Hello 2, world!</span>
       
        <button onClick={ e => alert("Hola!") }>Presioname</button>
    </div>
), document.body);
