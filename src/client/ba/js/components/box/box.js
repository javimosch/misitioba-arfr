import cityImage from 'img/city.jpg'
import 'img/ba-logo-round.png'
import './box.css';
import './box.scss';
import Data from 'common/res/files/data-example.xml';

if (module.hot) {
    module.hot.accept();
    module.hot.dispose(function() {
        window.$('.box').remove();
    });
}


let div = document.createElement('div');
div.className = 'box';

let text = document.createElement('h1');
text.innerHTML = 'This is a nice title 4';
div.appendChild(text);

//var myIcon = new Image();
//myIcon.src = cityImage;
//div.appendChild(myIcon);

console.log('Appending box', div);

document.body.appendChild(div);


let wait = (seconds) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => { resolve() }, seconds * 1000);
    });
};

(async() => {
    console.log('Waiting 5 seconds to show xml data');
    await wait(5);
    console.log(Data);
})();
