module.exports = {
    name: "modules",
    def: {
        name: {
            type: String,
            index: true,
            unique: true,
            required: true
        }
    },
    configure: (schema) => schema
};
